const url = {
    "base_url":"http://primecareers.co.in:5001/api",
    "home_url":"http://localhost:3000",
	"download_url":"http://172.104.40.142:5000/",
	"download_url2":"http://172.104.40.142:5000",
    "login_url":"http://localhost:3001",
    "captcha_key":"6LfhwZkUAAAAAIowLugY-6WbS3WWjhMeI94UaiCG",
    "grades":["A+","A","A-","B+","B","B-","C+","C","C-","D+","D","D-","E+","E","E-","F"]
};
export default url;